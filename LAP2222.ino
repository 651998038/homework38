#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Ticker.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN D4
#define DHTTYPE DHT11
DHT_Unified dht(DHTPIN, DHTTYPE);
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 32  // OLED display height, in pixels

#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define Analog A0
#define Light D0
#define Dirt D3
#define Fon D7
#define RedPin D10
#define GreenPin D9
#define RedLed D8
#define Green_Led 10


Ticker timer250;
volatile bool timer250check = false;
void time250() {
  timer250check = true;
}


Ticker timer1000;
volatile bool timer1000check = false;
void time1000() {
  timer1000check = true;
}

Ticker timer10000;
volatile bool timer10000check = false;
void time10000() {
  timer10000check = true;
}

Ticker timerL;
volatile bool timerLight = false;
void timeL1() {
  timerLight = true;
}
Ticker timerD;
volatile bool timerDirt = false;
void timeD1() {
  timerDirt = true;
}
Ticker timerF;
volatile bool timerFon = false;
void timeF1() {
  timerFon = true;
}


volatile bool red1 = false;
ICACHE_RAM_ATTR void redInterrupt() {
  red1 = true;
}
volatile bool Green1 = false;
ICACHE_RAM_ATTR void GreenInterrupt() {
  Green1 = true;
}
const int pingPin = D5;
int inPin = D6;


long microsecondsToCentimeters(long microseconds) {

  return microseconds / 29 / 2;
}


void setup() {
  Serial.begin(9600);
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }

  pinMode(Analog, INPUT);
  pinMode(RedPin, INPUT_PULLUP);
  pinMode(GreenPin, INPUT_PULLUP);

  pinMode(Light, OUTPUT);
  pinMode(Dirt, OUTPUT);
  pinMode(Fon, OUTPUT);
  pinMode(RedLed, OUTPUT);
  pinMode(Green_Led, OUTPUT);


  digitalWrite(Light, LOW);
  digitalWrite(Dirt, LOW);
  digitalWrite(Fon, LOW);
  digitalWrite(RedLed, LOW);
  digitalWrite(Green_Led, LOW);


  attachInterrupt(digitalPinToInterrupt(RedPin), redInterrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(GreenPin), GreenInterrupt, RISING);
  display.clearDisplay();
  
  timer250.attach(1, time250);
  timer1000.attach(1, time1000);
  timer10000.attach(2, time10000);
  timerL.attach(1, timeL1);
  timerD.attach(1, timeD1);
  timerF.attach(1, timeF1);
}

int count = 0;
float temp = 0;
float humi = 0;
int Moi = 50;
int Light1 = 0;
int Fon1 = 0;
int Moilevel = 0;
int loopCount = 0;
long duration, cm;
void loop() {
  display.clearDisplay();
  if (timer250check) {
    sensors_event_t event;
    dht.temperature().getEvent(&event);
    temp = event.temperature;
    dht.humidity().getEvent(&event);
    humi = event.relative_humidity;


    pinMode(pingPin, OUTPUT);
    digitalWrite(pingPin, LOW);
    delayMicroseconds(2);
    digitalWrite(pingPin, HIGH);
    delayMicroseconds(5);
    digitalWrite(pingPin, LOW);
    pinMode(inPin, INPUT);
    duration = pulseIn(inPin, HIGH);
    cm = microsecondsToCentimeters(duration);
    Serial.print(cm);
    Serial.print("cm");
    Serial.println();

    display.setTextSize(1);
    display.setTextColor(WHITE);



    display.setCursor(67, 5);
    display.print("L:");
    display.setCursor(78, 5);
    display.print(Light1);
    if(cm > 58){
      display.print(" ON");
    }else{
      display.print(" OFF");
    }

    display.setCursor(67, 15);
    display.print("D:");
    display.setCursor(78, 15);
    display.print(Moilevel);
    display.print(" %");

    display.setCursor(5, 5);
    display.print("C:");
    display.setCursor(17, 5);
    display.print(temp);
    display.print(" C");

    display.setCursor(5, 15);
    display.print("H:");
    display.setCursor(17, 15);
    display.print(humi);
    display.print(" %");

    display.setCursor(5, 25);
    display.print("R:");
    display.setCursor(17, 25);
    display.print(cm);
    display.print("%");

    display.setCursor(40, 25);
    display.print(" |M:");
    display.print(Moi);
    display.print(" %|");
    display.print("F:");
    display.print(Fon1);
    display.display();
    display.clearDisplay();
    timer250check = false;
  }

  if (timerLight) {
    loopCount = 100;
    if (loopCount == 100) {
      digitalWrite(Light, HIGH);
      delay(100);
      Light1 = 1024 - analogRead(Analog);
      Serial.println(Light1);
      digitalWrite(Light, LOW);
      delay(100);
      loopCount = 200;
    }
    if (loopCount == 200 && digitalRead(Light) == LOW) {
      digitalWrite(Dirt, HIGH);
      delay(100);
      Moilevel = (1024 - analogRead(Analog));
      digitalWrite(Dirt, LOW);
      delay(100);
      loopCount = 300;
    }
    if (loopCount == 300 && digitalRead(Dirt) == LOW) {
      digitalWrite(Fon, HIGH);
      delay(100);
      Serial.print("Fon is : ");
      Fon1 = (1024 - analogRead(Analog));
      digitalWrite(Fon, LOW);
      delay(100);
      loopCount = 100;
    }
    timerLight = false;
  }
  if (timer1000check) {
    if (red1) {
      Moi = Moi - 10;
      red1 = false;
    }
    if (Green1) {
      Moi = Moi + 10;
      Green1 = false;
    }
    timer1000check = false;
  }
  if (timerDirt) {
    if (Moilevel < Moi && Light1 > 238 && Fon1 < 138 && cm < 58) {
      digitalWrite(RedLed, HIGH);
      digitalWrite(Green_Led, LOW);
    } else if (cm > 58) {
      digitalWrite(Green_Led, HIGH);
      digitalWrite(RedLed, LOW);
    } else {
      digitalWrite(RedLed, LOW);
      digitalWrite(Green_Led, LOW);
    }
    timerDirt = false;
  }
}
